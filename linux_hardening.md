## Introduction
Linux depend on GRUB passwd to protect a system. Mnay tool help achieve this; one of them is this:
```
# grub-mkpasswd-pbkdf2
Enter password: 
Reenter password: 
PBKDF2 hash of your password is grub.pbkdf2.sha512.10000.C8F98127F738B448A4BE2631E56388E4AC71DED72729C2CE742351658DA2A11FCE090CBDE75E39E9B8B0D766D48A7DAAD00CAE5B3D2776D31B0C53EA3F2291D0.97FD3DFA32AA5B8AF8BBFA8932F3754584583CA8FDEE530C5C52394222B35C0E045C045AC0AD616799DDB7C51706EE5FD6087D2EA7238D95312DF9F5B02E6AED

```

## Filesystem Partitioning and Encryption
There are various software systems and tools that provide encryption to Linux systems. Since many modern Linux distributions ship with LUKS (Linux Unified Key Setup), let’s cover it in more detail.

When a partition is encrypted with LUKS, the disk layout would look as shown in the figure below.

LUKS disk layout: LUKS Partition Header, KM1, KM2, ..., KM8, Bulk Data

We have the following fields:

- LUKS phdr: It stands for LUKS Partition Header. LUKS phdr stores information about the UUID (Universally Unique Identifier), the used cipher, the cipher mode, the key length, and the checksum of the master key.
- KM: KM stands for Key Material, where we have KM1, KM2, …, KM8. Each key material section is associated with a key slot, which can be indicated as active in the LUKS phdr. When the key slot is active, the associated key material section contains a copy of the master key encrypted with a user's password. In other words, we might have the master key encrypted with the first user's password and saved in KM1, encrypted with the second user's password and saved in KM2, and so on.
- Bulk Data: This refers to the data encrypted by the master key. The master key is saved and encrypted by the user's password in a key material section.

LUKS reuses existing block encryption implementations. The pseudocode to encrypt data uses the following syntax:
```
enc_data = encrypt(cipher_name, cipher_mode, key, original, original_length)
```
The user-supplied password is used to derive the encryption key; the key is derived using password-based key derive function 2 (PBKDF2).

```
key = PBKDF2(password, salt, iteration_count, derived_key_length)
```



To decrypt data and restore the original plaintext, LUKS uses the following syntax:
```
original = decrypt(cipher_name, cipher_mode, key, enc_data, original_length)
```
Most distributions let you encrypt a drive using a graphical interface. However, if you would like to set up LUKS from the command line, the steps are along these lines:
- Install `cryptsetup-luks`. (You can issue `apt install cryptsetup`, `yum install cryptsetup-luks` or `dnf install cryptsetup-luks` for Ubuntu/Debian, RHEL/Cent OS, and Fedora, respectively.)
- Confirm the partition name using `fdisk -l`, `lsblk` or `blkid`. (Create a partition using `fdisk` if necessary.)
- Set up the partition for LUKS encryption: `cryptsetup -y -v luksFormat /dev/sdb1`. (Replace `/dev/sdb1` with the partition name you want to encrypt.)
- Create a mapping to access the partition: `cryptsetup luksOpen /dev/sdb1 EDCdrive`.
- Confirm mapping details: `ls -l /dev/mapper/EDCdrive` and `cryptsetup -v status EDCdrive`.
- Overwrite existing data with zero: `dd if=/dev/zero of=/dev/mapper/EDCdrive`.
- Format the partition: `mkfs.ext4 /dev/mapper/EDCdrive -L "Strategos USB"`.
- Mount it and start using it like a usual partition: `mount /dev/mapper/EDCdrive /media/secure-USB`.
  
In the terminal below, we show a real example of encrypting a USB flash memory that initially has one partition at `/dev/sdb1` in NTFS format.

If you want to check the LUKS setting, you can issue the command `cryptsetup luksDump /dev/sdb1`. In the terminal output below, we can see the UUID of the encrypted disk. We can also see that the cipher used is `aes-xts-plain64`. As for the key, PBKDF2 used SHA256 with the provided salt for 194180 iterations.
```
root@AttackBox# sudo cryptsetup luksDump /dev/sdb1
LUKS header information
Version:        2
Epoch:          3
Metadata area:  16384 [bytes]
Keyslots area:  16744448 [bytes]
UUID:           41199bad-d753-4dce-9284-0a81d27acc95
Label:          (no label)
Subsystem:      (no subsystem)
Flags:          (no flags)

Data segments:
  0: crypt
    offset: 16777216 [bytes]
    length: (whole device)
    cipher: aes-xts-plain64
    sector: 512 [bytes]

Keyslots:
  0: luks2
    Key:        512 bits
    Priority:   normal
    Cipher:     aes-xts-plain64
    Cipher key: 512 bits
    PBKDF:      argon2id
    Time cost:  4
    Memory:     965922
    Threads:    4
    Salt:       bd 45 40 96 27 93 cd fa 50 60 f4 28 d4 d8 b2 bd 
                58 69 72 72 35 2f 26 9c a8 14 ef 91 04 b2 dc cd 
    AF stripes: 4000
    AF hash:    sha256
    Area offset:32768 [bytes]
    Area length:258048 [bytes]
    Digest ID:  0
Tokens:
Digests:
  0: pbkdf2
    Hash:       sha256
    Iterations: 194180
    Salt:       6e e1 70 a4 3f d6 71 44 c8 e6 84 4d 99 51 7d c9 
                49 66 bf 37 61 b8 c3 d2 4e aa f7 25 27 e2 b3 8a 
    Digest:     c1 87 99 a1 d1 7a 05 8a ca cd 13 74 f0 33 ef 3a 
                98 c9 d7 a8 70 93 e2 ac 07 0f 2a 5c 89 f1 18 1d 
```

We cannot attach external storage to the VM, so we have created a `/home/tryhackme/secretvault.img` file instead. It is encrypted with the password `2N9EdZYNkszEE3Ad`. To access it, you need to open it using `cryptsetup` and then mount it to an empty directory, such as `myvault`. What is the flag in the secret vault?

## Firewall
Let’s briefly revisit the client/server model before we start. Any networked device can be a client, a server, or both simultaneously. The server offers a service, and the client connects to the server to use it. Examples of servers include web servers (HTTP and HTTPS), mail servers (SMTP(S), POP3(S), and IMAP(S)), name servers (DNS), and SSH servers. A server listens on a known TCP or UDP port number awaiting incoming client connection requests. The client initiates the connection request to the listening server, and the server responds to it.

## Linux Firewalls
The first Linux firewall was a packet filtering firewall, i.e., a stateless firewall. A stateless firewall can inspect certain fields in the IP and TCP/UDP headers to decide upon a packet but does not maintain information about ongoing TCP connections. As a result, a packet can manipulate a few TCP flags to appear as if it is part of an ongoing connection and evade certain restrictions. Current Linux firewalls are stateful firewalls; they keep track of ongoing connections and restrict packets based on specific fields in the IP and TCP/UDP headers and based on whether the packet is part of an ongoing connection.

The IP header fields that find their way into the firewall rules are:

1. Source IP address
2. Destination IP address
   
The TCP/UDP header fields that are of primary concern for firewall rules are:

1. Source TCP/UDP port
2. Destination TCP/UDP port

It is worth noting that it is impossible to allow and deny packets based on the process but instead on the port number. If you want the web browser to access the web, you must allow the respective ports, such as ports 80 and 443. This limitation differs from MS Windows’ built-in firewall, which can restrict and allow traffic per application.

## Netfilter
At the very core, we have netfilter. The netfilter project provides packet-filtering software for the Linux kernel 2.4.x and later versions. The netfilter hooks require a front-end such as iptables or nftables to manage.

In the following examples, we use different front-ends to netfilter in order to allow incoming SSH connections to the SSH server on our Linux system. As shown in the figure below, we want our SSH server to be accessible to anyone on the Internet with an SSH client.
![alt text](image.png)

## iptables
As a front-end, iptables provides the user-space command line tools to configure the packet filtering rule set using the netfilter hooks. For filtering the traffic, `iptables` has the following default chains:

- Input: This chain applies to the packets incoming to the firewall.
- Output: This chain applies to the packets outgoing from the firewall.
- Forward This chain applies to the packets routed through the system.

Let’s say that we want to be able to access the SSH server on our system remotely. For the SSH server to be able to communicate with the world, we need two things:

1. Accept incoming packets to TCP port 22.
2. Accept outgoing packets from TCP port 22.

Let’s translate the above two requirements into `iptables` commands:

```
iptables -A INPUT -p tcp --dport 22 -j ACCEPT
```

`-A INPUT` appends to the INPUT chain, i.e., packets destined for the system.
`-p tcp --dport 22` applies to TCP protocol with destination port 22.
`-j ACCEPT` specifies (jump to) target rule ACCEPT.

```
iptables -A OUTPUT -p tcp --sport 22 -j ACCEPT
```

`-A OUTPUT` append to the OUTPUT chain, i.e., packets leaving the system.
`-p tcp --sport 22` applies to TCP protocol with source port 22.

Let’s say you only want to allow traffic to the local SSH server and block everything else. In this case, you need to add two more rules to set the default behaviour of your firewall:

- iptables -A INPUT -j DROP to block all incoming traffic not allowed in previous rules.
- iptables -A OUTPUT -j DROP to block all outgoing traffic not allowed in previous rules.
  
In brief, the rules below need to be applied in the following order: